#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>


#define BUFFER_MAX_VALUE 100
#define LOG_DELAY 15

int counter = 0;
int producer_sleeps = 1, consumer_sleeps = 1;
time_t startTime;


void logCounter(char* templ) {
        if (time(NULL) - startTime > LOG_DELAY) {
                printf(templ, counter);
                startTime = time(NULL);
        }
}


void* producerPayload(void *args) {
	while (1) {
		if (counter == BUFFER_MAX_VALUE) {
			logCounter("Counter in producer delay: %d\n");
			continue; // Sleeping
		}
		counter++;
	}
}

void* consumerPayload(void *args) {
	while (1) {
		if (counter == 0) {
			logCounter("Counter in consumer delay: %d\n");
			continue; // Sleeping
		}
		counter--;
	}
}



int main() {
	pthread_t producer, consumer, display;
	startTime = time(NULL);

	pthread_create(&producer, NULL, producerPayload, (void *) NULL);
	pthread_create(&consumer, NULL, consumerPayload, (void *) NULL);

	// while (1);
	
	pthread_join(producer, NULL);
	pthread_join(consumer, NULL);
}
