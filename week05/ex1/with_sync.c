#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define NUM_OF_THREADS 8

void *payload(int id) {
	// Some payload
	printf("Thread%d is working...\n", id);
	int cnt = 0;
	for (int i = 0; i < 1000000; i++) {
		cnt++;
	}

	printf("Exiting Thread%d\n", id);
}


int main() {
	for (int i = 0; i < NUM_OF_THREADS; i++) {
		printf("Creating Thread%d\n", i);
		pthread_t thr;
		pthread_create(&thr, NULL, payload, i);
		pthread_join(thr, NULL);
	}
}
