#include <stdio.h>
#include <stdlib.h>


const int M = 3; // number of resource classes
const int N = 5; // number of processes
int R[N][M], C[N][M]; // R - requested resources, C - current allocated resources
int E[M], A[M]; // E - number of resources in existence, A - number of available resources

int T[M]; // terminated resources

// IMPORTANT: I assume that there are no errors in input
// In particular: Current allocated + Available is less or equal then the number resources in existence

int terminate_ready_process() {
	int updated = 0;

	for (int i = 0; i < N; i++) {
		if (T[i] == 1) continue; // skipping terminated processes

		// Check that OS is ready to provide all requested resources to the process
		int p_ready = 1;
		for (int j = 0; j < M; j++) {
			if (R[i][j] > A[j]) {
				p_ready = 0;
				break;
			}
		}
		if (p_ready) {
			// free allocated for the process resources
			for (int j = 0; j < M; j++) {
				A[j] += C[i][j] + R[i][j];
				C[i][j] = 0;
				R[i][j] = 0;
			}

			// Terminate processes
			T[i] = 1;
			updated = 1;
			break;
		}
	}

	return updated;
}

int num_of_terminated() {
	int terminated = 0;
	for (int i = 0; i < M; i++) terminated += T[i];
	return terminated;
}

void initialize() {
	for (int i = 0; i < M; i++) T[i] = 0;
}

void read_input(FILE *file) {
	// Reading resources in existence
	for (int i = 0; i < M; i++) fscanf(file, "%d", &E[i]);

	// Reading available resources
	for (int i = 0; i < M; i++) fscanf(file, "%d", &A[i]);

	// Reading current allocation matrix
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			fscanf(file, "%d", &C[i][j]);
		}
	}

	// Readnig request matrix
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			fscanf(file, "%d", &R[i][j]);
		}
	}
}

int main() {
	initialize();

	FILE *file;

	// opening input file
	if ((file = fopen("INPUT.TXT", "r")) == NULL) {
		printf("Input file is not readable or not exists");
		exit(1);
	}

	read_input(file);
	fclose(file);

	while(terminate_ready_process()){};

	if (num_of_terminated() == M) {
		printf("All processes terminated successfully\n");
	} else {
		printf("Deadlock detected!\n");
	}

	return 0;
}
