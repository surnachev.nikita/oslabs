#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>


int main() {
	DIR *dirp = opendir("/");
	if (dirp == NULL) {
		printf("Error caused while open the directory.");
		return 0;
	}

	struct dirent* entry;
	int i = 0;
	while ((entry = readdir(dirp)) != NULL) {
		printf("%d: %s\n", ++i, entry->d_name);
	}
}
