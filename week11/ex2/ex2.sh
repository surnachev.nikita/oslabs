# For repeating experiment
sudo umount lofsdisk 2> /dev/null
sudo rm -r lofsdisk 2> /dev/null
sudo rm lofs.img 2> /dev/null

fallocate -l 100M lofs.img
mkfs.ext4 lofs.img

mkdir lofsdisk
sudo mount -o loop lofs.img lofsdisk

sudo touch lofsdisk/test_file1.txt
sudo touch lofsdisk/test_file2.txt

# Adding binaries and shared libraries of ls, cmd, cat, echo in new filesystem
sudo mkdir lofsdisk/bin
sudo mkdir lofsdisk/lib
sudo mkdir lofsdisk/lib64
sudo mkdir lofsdisk/lib/aarch64-linux-gnu
sudo cp /bin/bash lofsdisk/bin
sudo cp /bin/ls lofsdisk/bin
sudo cp /bin/echo lofsdisk/bin
sudo cp /bin/cat lofsdisk/bin
sudo cp /bin/pwd lofsdisk/bin

list="$(ldd /bin/bash | egrep -o '/lib.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/bash | egrep -o '/lib64.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/cat | egrep -o '/lib.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/cat | egrep -o '/lib64.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/echo | egrep -o '/lib.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/echo | egrep -o '/lib64.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/ls | egrep -o '/lib.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/ls | egrep -o '/lib64.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/pwd | egrep -o '/lib.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done
list="$(ldd /bin/pwd | egrep -o '/lib64.*\.[0-9]')"
for i in $list; do sudo cp  -v "$i" "lofsdisk${i}"; done

# Compiling simge c++ script and copying executable in new filesystem
gcc ex2.c
sudo cp a.out lofsdisk/

# Running ls and pwd in new filesystem
sudo chroot lofsdisk ls
sudo chroot lofsdisk pwd


echo "OUTPUT FROM MOUNTED FILESYSTEM" > ex2.txt
sudo chroot lofsdisk /a.out >> ex2.txt

echo "OUTPUT FROM HOST FILESYSTEM" >> ex2.txt
./a.out >> ex2.txt
