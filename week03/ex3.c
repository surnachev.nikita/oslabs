#include <stdio.h>
#include <stdlib.h>

struct Node {
    int value;
    struct Node* next;
};
typedef struct Node* List;

List create(int value) {
    List node = (List) malloc(sizeof(struct Node));
    node->value = value;
    node->next = NULL;
    return node;
}

void printl(List* head) {
    List current = *head;
    while (current != NULL) {
        printf("%d ", current->value);
        current = current->next;
    }
    putchar('\n');
}

void insert(List* tail, List node) {
    if (*tail == NULL) return;
    if ((*tail)->next == NULL) (*tail)->next = node;
    else {
        node->next = (*tail)->next;
        (*tail)->next = node;
    }
}

void delete(List* head, List node) {
    if (*head == node) {
        node->next = NULL;
        return;
    }
    List current = *head;
    List previous;

    while (current != NULL && current != node) {
        previous = current;
        current = current->next;
    }

    if (current == NULL) return;

    previous->next = current->next;
    current->next = NULL;
}

int main() {
    List head = create(0);
    printf("Create 0 node: ");
    printl(&head);

    List second = create(1);
    insert(&head, second);
    printf("Insert 1: ");
    printl(&head);

    delete(&head, second);
    printf("Delete 1: ");
    printl(&head);
    return 0;
}

