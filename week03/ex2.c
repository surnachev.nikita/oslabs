#include <stdio.h>

void bubble_sort(int*, int);

int main() {

    int a[10] = {3, 1, 2, 4, 1, 3, 4};

    bubble_sort(a, 10);

    printf("sorted:\n");
    for (int i = 0; i < 10; i++) {
        printf("%d ", a[i]);
    }

    return 0;

}

void bubble_sort(int* a, int len) {
    for (int i = 0; i < len - 1; i++) {
        for (int j = len - 1; j > i; j--) {
            if (a[j] < a[j - 1]) {
                int temp = a[j];
                a[j] = a[j - 1];
                a[j - 1] = temp;
            }
        }
    }
}