#include <stdio.h>
#include <stdlib.h>

#define PROC_MAX_NUM 1000

int max(int a, int b) {
	return (a > b ? a : b);
}

int min(int a, int b) {
	return (a < b ? a : b);
}

typedef struct Proc {
	int id;
	int CT;  // Completion time
	int TAT; // Turn around time
	int WT;  // Waiting time
	int BT;	 // Burst time
	int AT;  // Arrival time
	int served; 

	int lastStart;
} Proc; 


// Comparator for qsort (by burst time)
int compareProcesses(const void *a1, const void *a2) {
	Proc proc1 = *(const Proc*)a1;
	Proc proc2 = *(const Proc*)a2;
	
	return proc1.BT - proc2.BT;
}


void printPipelineMetrics(Proc *processes, int n) {
	float averageTAT = 0.0, averageWT = 0.0;
	for (int i = 0; i < n; i++) {
		averageTAT += processes[i].TAT;
		averageWT += processes[i].WT;
	}

	averageTAT = averageTAT / n;
	averageWT = averageWT / n;

	printf("--PIPELINE METRICS--\n");
	printf("Average Turn Around Time: %f\nAverage Waiting Time: %f\n", 
		averageTAT,
		averageWT
	);  
}


void printProcessesMetrics(Proc *processes, int n) {
	for (int i = 0; i < n; i++) {
		printf("--PROCESS #%d:--\n", i);
		printf("TAT: %d\n", processes[i].TAT);
		printf("WT: %d\n", processes[i].WT);
		printf("BT: %d\n", processes[i].BT);
		printf("AT: %d\n", processes[i].AT);
	}
}


int main() {
	int numOfProcesses, quantum;
	printf("Number of processes: ");
	scanf("%d", &numOfProcesses);

	printf("Quantum: ");
	scanf("%d", &quantum);

	Proc *processes = malloc(numOfProcesses * sizeof(Proc));

	printf("---INSERT PROCESS INFORMATION---\n");
	int maxArrivalTime = 0;
	
	for (int i = 0; i < numOfProcesses; i++) {
		printf("PROCESS #%d\n", i);	
		processes[i].id = i;
		
		printf("Burst time: ");
		scanf("%d", &processes[i].BT);	
		
		printf("Arrival time: ");
		scanf("%d", &processes[i].AT);
		
		processes[i].served = 0;
		processes[i].lastStart = 0;

		maxArrivalTime = max(maxArrivalTime, processes[i].AT);
	} 

	qsort(processes, numOfProcesses, sizeof(Proc), compareProcesses);
	
	int time = 0, doneProcesses = 0;
	while (doneProcesses != numOfProcesses) {
		int chosed = 0;
		for (int i = 0; i < numOfProcesses; i++) {
			if (processes[i].AT >= time && processes[i].BT < processes[i].served) {
				int addTime = min(quantum, processes[i].BT - processes[i].served);
				processes[i].served = min(processes[i].BT, processes[i].served + quantum);
				processes[i].WT += time - processes[i].lastStart;				
				processes[i].lastStart = time + addTime;				

				if (processes[i].served == processes[i].BT) {
					processes[i].CT = time + addTime;
					processes[i].TAT = processes[i].CT - processes[i].AT;	
					doneProcesses++;
				}
				
				time += addTime;
				chosed = 1; 
				break;
			}			
		}
		printf("DP: %d\n", doneProcesses);
		if (chosed == 0) time++;
	}

	printProcessesMetrics(processes, numOfProcesses);
	printPipelineMetrics(processes, numOfProcesses);
	free(processes);
}
