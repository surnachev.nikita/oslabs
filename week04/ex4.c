  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


const int INPUT_SIZE = 1007;

int main(int argc, char* argv[]) {
    char input[INPUT_SIZE];

    // Uses only first argument
    char *new_arguments[] = {argv[1], NULL};
    char *new_environment[] = {NULL};
    execve(argv[1], new_arguments, new_environment);

    while (1) {        
        scanf("%s", input);
        if (strcmp(input, "^Z") == 0) return 0;
        system(input);
    }
	
    printf("Bye!\n");
}
