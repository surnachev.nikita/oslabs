
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define _XOPEN_SOURCE 600


int main() {
    int n = rand() % 100;
    int pid = fork();

    // Child process
    if (pid == 0) printf("Hello from child %d - %d\n", pid, n);
    // Parent process
    else printf("Hello from parent %d - %d\n", pid, n);

    return 0;
}

/*
Running ex1.sh produce output:
Hello from parent 11001 - 7
Hello from child 0 - 7
Hello from parent 11003 - 7
Hello from child 0 - 7
Hello from parent 11005 - 7
Hello from child 0 - 7
Hello from parent 11007 - 7
Hello from child 0 - 7
Hello from parent 11009 - 7
Hello from child 0 - 7
Hello from parent 11011 - 7
Hello from child 0 - 7
Hello from parent 11013 - 7
Hello from child 0 - 7
Hello from parent 11015 - 7
Hello from child 0 - 7
Hello from parent 11017 - 7
Hello from child 0 - 7
Hello from parent 11019 - 7
Hello from child 0 - 7


We can see that parent's pid differs every run, because it is unique
But child's pin always equals zero 

*/
