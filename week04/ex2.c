#include <stdio.h>
#include <unistd.h>
#include <unistd.h>

#define _XOPEN_SOURCE 600

int main() {
    for (int i = 0; i < 5; i++) {
        fork();
        sleep(5);
    }

    return 0;

}

/*
First run: 8 processes, 3 repetitions.
a.out───a.out
a.out─┬─a.out───a.out
      └─a.out
a.out─┬─a.out─┬─a.out───a.out
      │       └─a.out
      ├─a.out───a.out
      └─a.out             
Every fork() doubles calls -> binary tree


Second run: 32 processes and 5 iterations
a.out─┬─a.out─┬─a.out─┬─a.out─┬─a.out───a.out
      │       │       │       └─a.out
      │       │       ├─a.out───a.out
      │       │       └─a.out
      │       ├─a.out─┬─a.out───a.out
      │       │       └─a.out
      │       ├─a.out───a.out
      │       └─a.out
      ├─a.out─┬─a.out─┬─a.out───a.out
      │       │       └─a.out
      │       ├─a.out───a.out
      │       └─a.out
      ├─a.out─┬─a.out───a.out
      │       └─a.out
      ├─a.out───a.out
      └─a.out
*/
