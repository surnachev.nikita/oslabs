#include <stdio.h>
#include <stdlib.h>
#include <string.h>


const int INPUT_SIZE = 1007;


int main() {
    char userInput[INPUT_SIZE];

    while (1) {
        scanf("%s", userInput);

        if (strcmp(userInput, "^Z") == 0) return 0;

        system(userInput);
    }

    printf("Bye!\n");
}
