#include <stdio.h>
#include <stdlib.h>

typedef int bool;
#define TRUE  1
#define FALSE 0

typedef struct {
    int page_number;
    int counter;
    bool rbit;
} page;

typedef struct ll_node {
    page *page;
    struct ll_node *next;
    struct ll_node *prev;
} linked_list_node;


typedef struct ll {
    linked_list_node *node_start;
    linked_list_node *node_end;
    int size;
} linked_list;


void delete_node(linked_list* list, linked_list_node* node) {
    // TODO: check that node belongs to the linked list
    // (In this case we have only one linked list, so it is not critical)
    node->prev->next = node->next;
    node->next->prev = node->prev;
    free(node->page);
    free(node);
    list->size--;
}


void delete_list(linked_list *list) {
    for (linked_list_node* node = list->node_start->next; node != list->node_end;) {
        linked_list_node* next_node = node->next;
        delete_node(list, node);
        node = next_node;
    }
    free(list->node_start);
    free(list->node_end);
}

void add_page(linked_list* ll, page* p) {
    linked_list_node *node = malloc( sizeof(linked_list_node) );
    node->page = p;

    ll->node_end->prev->next = node;
    node->prev = ll->node_end->prev;
    node->next = ll->node_end;
    ll->node_end->prev = node;

    ll->size++;
}

linked_list *create_linked_list() {
    linked_list* list = malloc( sizeof(linked_list) );
    list->node_start = malloc( sizeof(linked_list_node) );
    list->node_end = malloc( sizeof(linked_list_node) );

    list->node_start->next = list->node_end;
    list->node_end->prev = list->node_start;
    list->size = 0;
    return list;
}

linked_list_node *get_node_by_page_number(linked_list* list, int page_number) {
    for (linked_list_node* node = list->node_start->next; node != list->node_end; node = node->next) {
        if (node->page->page_number == page_number) {
            return node;
        }
    }
    return NULL;
}

bool exists_with_page_number(linked_list *list, int page_number) {
    return get_node_by_page_number(list, page_number) != NULL;
}

linked_list_node *get_nfu_node(linked_list *list) {
    linked_list_node *nfu_node = list->node_start->next;
    for (linked_list_node* node = nfu_node->next; node != list->node_end; node = node->next) {
        if (node->page->counter < nfu_node->page->counter ||
           (node->page->counter == nfu_node->page->counter &&
                 node->page->page_number < nfu_node->page->page_number)) {
            nfu_node = node;
        }
    }
    return nfu_node;
}

void update_counter(page* p) {
    p->counter = p->counter >> 1;
    p->counter += (p->rbit << 7);
    p->rbit = 0;
}

void release_rbits(linked_list* list) {
    for (linked_list_node *node = list->node_start->next; node != list->node_end; node = node->next)
        update_counter(node->page);
}

page *create_page(int page_number) {
    page *p = malloc(sizeof(page));
    p->counter = 0;
    p->page_number = page_number;
    p->rbit = 0;
    return p;
}

void set_rbit(linked_list *list, int page_number) {
    linked_list_node *node = get_node_by_page_number(list, page_number);
    node->page->rbit = 1;
}


int main() {
    linked_list *page_list = create_linked_list();

    int page_frames, memory_accesses;
    scanf("%d%d", &page_frames, &memory_accesses);

    int misses = 0;
    int cycle_number, page_number;

    int prev_cycle = -1;
    bool b;
    for (int i = 0; i < memory_accesses; i++) {
        b = TRUE;
        scanf("%d%d", &cycle_number, &page_number);

        if (prev_cycle != -1 && prev_cycle != cycle_number) release_rbits(page_list);
        prev_cycle = cycle_number;

        if (!exists_with_page_number(page_list, page_number)) {
            if (page_list->size == page_frames) delete_node(page_list, get_nfu_node(page_list));
            add_page(page_list, create_page(page_number));
            b = FALSE;
            misses++;
        }
        set_rbit(page_list, page_number);

        printf("%d\n", b);
    }
    printf("%f\n", (float)(memory_accesses - misses) / (float)misses);
    delete_list(page_list);

    return 0;
}
