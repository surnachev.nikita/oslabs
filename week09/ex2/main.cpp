#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main() {
	freopen("test.txt", "r", stdin);
	freopen("test_modified.txt", "w", stdout);

	int frame_pages, requests;
	cin >> frame_pages >> requests;

	vector <int> page_numbers;
	vector <int> clock_cycles;

	int clock_cycle, page_number;
	for (int i = 0; i < requests; i++) {
		cin >> clock_cycle >> page_number;
		page_numbers.push_back(page_number);
		clock_cycles.push_back(clock_cycle);
	}

	sort(page_numbers.begin(), page_numbers.end());

	cout << frame_pages << " " << requests << endl;
	for (int i = 0; i < requests; i++) {
		cout << clock_cycles[i] << " " << page_numbers[i] << endl;
	}
}
