#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>

int main() {
	char dir_path[] = "tmp";

	DIR *dirp = opendir(dir_path);
	if (dirp == NULL) {
		printf("Error caused while open the directory.");
		return 0;
	}

	printf("FILE --- HARD LINKS\n");

	struct dirent* entry;
	while ( (entry = readdir(dirp)) != NULL ) {
		if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
		struct stat entry_stats;

		char *entry_path = calloc(strlen(dir_path) + strlen(entry->d_name) + 1, sizeof(char));

		sprintf(entry_path, "%s/%s", dir_path, entry->d_name);
		if (stat(entry_path, &entry_stats) != 0) continue;

		if (entry_stats.st_nlink > 1) {
			printf("%s - ", entry->d_name);
			struct dirent* entry2;
			DIR *dirp2 = opendir(dir_path);

			while ( (entry2 = readdir(dirp2)) != NULL ) {
				struct stat entry2_stats;
				char *entry2_path = calloc(strlen(dir_path) + strlen(entry2->d_name) + 1, sizeof(char));
				sprintf(entry2_path, "%s/%s", dir_path, entry2->d_name);
				if(stat(entry2_path, &entry2_stats) != 0) continue;

				if (entry_stats.st_ino == entry2_stats.st_ino) printf("%s ", entry2->d_name);
			}
			printf("\n");
		}

		free(entry_path);
	}

	closedir(dirp);
	return 0;
}
