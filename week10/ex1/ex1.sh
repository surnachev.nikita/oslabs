#!/bin/sh

echo "Nikita Surnachev" >> _ex1.txt

ln _ex1.txt _ex1_1.txt 
ln _ex1.txt _ex1_2.txt

echo "Step 1: i-node of file _ex1.txt"
ls -i _ex1.txt

echo "Step 2: i-node of file _ex1_1.txt"
ls -i _ex1_1.txt

echo "Step 3: i-node of file _ex1_2.txt"
ls -i _ex1_2.txt
