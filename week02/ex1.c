#include <stdio.h>
#include <limits.h>
#include <float.h>

int main() {
    int i = INT_MAX;
    float f = FLT_MAX;
    double d = DBL_MAX;

    printf("Integer size equals %lu bytes; Integer maximal value equals %d\n", sizeof(i), i);
    printf("Float size equals %lu bytes; Float maximal value equals %e\n", sizeof(f), f);
    printf("Double size equals %lu bytes; Double maximal value equals %e\n", sizeof(d), d);

    return 0;

}
