#include <stdio.h>

void shift(int n) {
    for (int i = 0; i < n; i++) {
        printf(" ");
    }
}

void printTree(int n) {
    int ls = 2 * n;
    for (int i = 1; i <= n; i++) {
        shift(ls - 2 * i);
        for (int j = 0; j < 2 * i - 1; j++) printf("* ");
        printf("\n");
    }
}

int main() {
    int n;
    scanf("%d", &n);
    printTree(n);
    return 0;
}


