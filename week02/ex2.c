#include <stdio.h>
#include <string.h>

void displayReversedString(char* s) {
    int len = strlen(s);
    for (int i = len - 1; i >= 0; i--) putchar(s[i]);
    putchar('\n');
}

int main() {
    char str[777];
    printf("Input a string: ");
    scanf("%s", str);
    printf("Reversed string: ");
    displayReversedString(str);
    return 0;
}
