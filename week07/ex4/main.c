#include <stdio.h>
#include <stdlib.h>
#include <memory.h>


void *nikitasRealloc(void* arr, size_t origSize, size_t newSize) {
	if (newSize == 0) {
		free(arr);
		return NULL;
	}

	void *newArr = malloc(newSize);
	if (arr == NULL) return newArr;

	memmove(newArr, arr, origSize < newSize ? origSize : newSize);
	free(arr);
	return newArr;
}

const int ARR_SIZE = 5;

int main() {
	int *array = (int *) calloc(ARR_SIZE, sizeof(int));
	for (int i = 0; i < ARR_SIZE; i++) array[i] = i;
	array = nikitasRealloc(array, sizeof(int) * ARR_SIZE, sizeof(int) * ARR_SIZE * 3);
	for (int i = ARR_SIZE; i < ARR_SIZE * 3; i++) array[i] = i * 10;
	
	for (int i = 0; i < ARR_SIZE * 3; i++) printf("%d ", array[i]);
	printf("\n");
	free(array);
}
