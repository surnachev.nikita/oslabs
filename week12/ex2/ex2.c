#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>


int main() {
	char *device = "/dev/input/by-path/pci-0000:00:03.0-usb-0:3:1.0-event-kbd";
	int fd = open(device, O_RDONLY);

	if (fd == -1) {
		printf("Error!\n");
		return EXIT_FAILURE;
	} else {
		struct input_event evt;

		while (1) {
			ssize_t b = read(fd, &evt, sizeof(evt));

			if (evt.type == EV_KEY) {
				if (evt.value != 1 && evt.value != 0) continue;
				printf("%s 0x%04x (%d)\n", evt.value == 1 ? "PRESSED" : "RELEASED", evt.code, evt.code);
			}
		}
	}
	return EXIT_SUCCESS;
}
