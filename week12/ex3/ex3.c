#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>

#define N 7777 // N should be bigger than max event code (https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/include/uapi/linux/input-event-codes.h)

int pressed[N];

int number_of_pressed() {
	int n = 0;
	for (int i = 0; i < N; i++) {
		n += pressed[i];
	}
	return n;
}

int main() {
	char *device = "/dev/input/by-path/pci-0000:00:03.0-usb-0:3:1.0-event-kbd";
	int fd = open(device, O_RDONLY);

	printf("Available shortcuts:\n");
	printf("P + E - I passed the Exam!\n");
	printf("C + A + P - Get some cappuchino!\n");
	printf("N + S - My name is Nikita Surnachev!\n");
	fflush(stdout);

	if (fd == -1) {
		printf("Error!\n");
		return EXIT_FAILURE;
	} else {
		struct input_event evt;

		while (1) {
			ssize_t b = read(fd, &evt, sizeof(evt));

			if (evt.type == EV_KEY) {
				if (evt.value != 1 && evt.value != 0) continue;

				if (evt.value == 1) pressed[evt.code] = 1;
				else pressed[evt.code] = 0;

				// PE shortcut 
				if (pressed[25] && pressed[18] && number_of_pressed() == 2) {
					printf("I passed the Exam!\n");
				}

				// CAP shortcut
				if (pressed[46] && pressed[30] && pressed[25] && number_of_pressed() == 3) {
					printf("Get some cappuchino!\n");
				}

				// NS shortcut
				if (pressed[31] && pressed[49]) {
					printf("My name is Nikita Surnachev!\n");
				}

				fflush(stdout);
			}
		}
	}
	return EXIT_SUCCESS;
}
